package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro zobrazení nápovědy ke hře.
 * Hráči se zobrazí stručná informace o cílu hry a také všechny příkazy, které lze použít.
 *
 * @author Jarmila Pavlíčková
 * @author Luboš Pavlíček
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandHelp implements ICommand
{
    private static final String NAME = "nápověda";

    private ListOfCommands listOfCommands;

    /**
     * Konstruktor třídy.
     *
     * @param listOfCommands odkaz na seznam příkazů, které je možné ve hře použít
     */
    public CommandHelp(ListOfCommands listOfCommands)
    {
        this.listOfCommands = listOfCommands;
    }

    /**
     * Metoda vrací obecnou nápovědu ke hře. Vypisuje vcelku primitivní
     * zprávu o herním příběhu a seznam dostupných příkazů, které může hráč používat.
     * Nejprve však kontroluje vstup.
     *
     * @param parameters parametry příkazu <i>(aktuálně se ignorují)</i>
     * @return nápověda, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "Nevím co to znamená. Příkaz '" + NAME + "' se používá bez parametrů.";
        }
        return "Vašim úkolem je dostat se do obchodního centra, zde nakoupit všechny věci ze seznamu (brufen, chléb, mýdlo).\n"
        + "Následně všechny tyto věci donést domů.\n"
        + "\n"
        + "Ve hře můžete používat tyto příkazy:\n"
        + listOfCommands.getNames();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
