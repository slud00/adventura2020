package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro pokládání věcí.
 *
 * @author Dominik Sluka
 * @version LS2020
 */
public class CommandPut implements ICommand
{
    private static final String NAME = "polož";

    private GamePlan plan;

    public CommandPut(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí položit předmět. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznámý předmět pro sebrání)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. nelze položit více předmětů najednou)</i>, vrátí chybové hlášení. 
     * Pokud byl zadán právě jeden parametr, zkontroluje, zda se daný předmět nachází v batohu. Pokud ne, vrátí
     * chybové hlášení.  Pokud všechny kontroly proběhnou v pořádku, vyndá
     * předmět z batohu, přidá ho do místnosti a nakonec zkontroluje zda náhodou tento příkaz nezpůsobil výhru.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám vyndat, musíte zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím vyndat více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!plan.getBackpack().isItemAtBag(itemName)) {
            return itemName + " není v batohu.";
        }

        Item item = plan.getBackpack().getItemFromBag(itemName);

        area.addItem(item);
        String result = plan.getBackpack().takeOutOfBackpack(item);

        if (plan.isVictorious(area)) {
            result = "Vyhrál jste.";
        }

        return result;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */@Override
    public String getName()
    {
        return NAME;
    }

}

