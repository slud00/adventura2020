package cz.vse.sluka.logika;

/**
 * Třída pro enumeraci typu pro způsob získání předmětu.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public enum GetType
{
    BUY, TAKE, TAKEOUT, OTHER
}
