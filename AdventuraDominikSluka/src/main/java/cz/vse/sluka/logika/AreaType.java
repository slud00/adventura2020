package cz.vse.sluka.logika;

/**
 * Třída pro enumeraci typů míst.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public enum AreaType
{
    RESTAURANT, BANK, SHOP, PARKING, GARAGE, HOME, OTHER
}
