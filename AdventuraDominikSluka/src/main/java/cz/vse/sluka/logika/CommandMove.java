package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro pohyb mezi herními lokacemi.
 * Kontroluje počet vstupů (akceptuje pouze jeden vstup) a dále také zda lze do místnosti projít z aktuální lokece.
 *
 * @author Jarmila Pavlíčková
 * @author Luboš Pavlíček
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandMove implements ICommand
{
    private static final String NAME = "jdi";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandMove(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí přesunout hráče do jiné lokace. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznáme cíl cesty)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. hráč chce jít na více míst
     * současně)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda s aktuální lokací sousedí jiná lokace s daným názvem <i>(tj.
     * zda z aktuální lokace lze jít do požadovaného cíle)</i>. Pokud ne, vrátí
     * chybové hlášení. Dále zkontroluje zda není hráč v autě, pokud ano vypíše chybové hlášení.
     * Nakonec zkontroluje zda se mezi zadanými lokacemi nedá pohybovat pouze autem.
     * Pokud ano, upozorní na to hráče.
     * Pokud všechny kontroly proběhnou v pořádku, provede přesun
     * hráče do cílové lokace, vrátí její popis a nastaví zrušení pozice u bankomatu
     * pro případ, že by byl hráč v bance u bankomatu.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nechápu, kam mám jít. Musíte mi zadat nějaký cíl.";
        } else if (parameters.length > 1) {
            return "Nechápu, co po mě chcete. Neumím se 'rozdvojit' a jít na více míst současně.";
        }

        String exitName = parameters[0];

        Area exitArea = plan.getCurrentArea().getExitArea(exitName);

        if (exitArea == null) {
            return "Tam se ale odsud jít nedá.";
        }

        if (plan.getCurrentArea().getInCar()) {
            return "Jste v autě. Nemůžete jít.";
        }

        if (plan.getCurrentArea().getByCar() && plan.getCurrentArea().getExitArea(exitName).getByCar()) {
            return "Tam můžete pouze jet.";
        }

        plan.getCurrentArea().setNextByATM(false);

        plan.setCurrentArea(exitArea);
        return exitArea.getFullDescription();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
