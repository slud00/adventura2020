package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro přistoupení k bankomatu
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandGoToATM implements ICommand
{
    private static final String NAME = "přistup";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandGoToATM(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí přesunout hráče k bankomatu. Nejprve zkontroluje správnost vstupu (zda je vstup zadán a zda není delší než 1). 
     * Dále zkontroluje zda se bankomat nachází v dané místnosti a zda vstup je bankomat, tudíž se k němu dá přistoupit.
     * Pokud již stojíte u bankomatu a pokusíte se k němu znovu přistoupit, vypíše chybovou hlášku.
     * Pokud vše projde v pořádku, nastaví, že hráč je u automatu.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, k čemu mám jít.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím jít na dvě místa současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!area.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }

        if (!"bankomat".equals(itemName)) {
            return "K "+ itemName + " nelze přistoupit.";
        }

        if (plan.getCurrentArea().getNextByATM()) {
            return "Už stojíte u bankomatu.";
        }

        plan.getCurrentArea().setNextByATM(true);
        return "Jsem u bankomatu.";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
