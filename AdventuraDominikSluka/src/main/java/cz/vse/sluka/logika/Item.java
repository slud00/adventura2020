package cz.vse.sluka.logika;

/**
 * Třída představující předměty ze hry. .
 *
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class Item
{
    private String name;
    private String description;
    private boolean moveable;
    private GetType getType;

    /**
     * Konstruktor třídy. Vytvoří předmět se zadaným jménem, popisem, typem předmětu a údajem o tom zda je přenositelný.
     *
     * @param name název předmětu
     * @param description podrobnější popis předmětu
     * @param moveable přenositelnost předmětu
     * @param getType způsob získání předmětu
     */
    public Item(String name, String description, GetType getType, boolean moveable)
    {
        this.name = name;
        this.description = description;
        this.moveable = moveable;
        this.getType = getType;
    }


    /**
     * Konstruktor třídy. Vytvoří předmět se zadaným jménem, popisem, typem předmětu a automaticky zadanou přenositelností (true).
     *
     * @param name název předmětu
     * @param description podrobnější popis předmětu
     * @param getType způsob získání předmětu
     */
    public Item(String name, String description, GetType getType)
    {
        this(name, description, getType, true);
    }

    /**
     * Metoda vrací název předmětu, který byl zadán při vytváření předmětu jako
     * parametr konstruktoru. 
     *
     * @return název předmětu
     */
    public String getName()
    {
        return name;
    }

    /**
     * Metoda vrací detailní informace o předmětu. Např. k čemu předmět slouží.
     *
     * @return detailní informace o předmětu
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Metoda pro nastavení detailní informace o předmětu. Např. k čemu předmět slouží.
     *
     * @param description podrobnější popis předmětu
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Metoda pro zjištění, zda je předmět přenositelný.
     * 
     * @return přenositelnost předmětu
     */
    public boolean isMoveable()
    {
        return moveable;
    }

    /**
     * Metoda pro nastavení přenositelnosti předmětu.
     * 
     * @param moveable přenositelnost předmětu
     */
    public void setMoveable(boolean moveable)
    {
        this.moveable = moveable;
    }

    /**
     * Metoda vrací typ získání předmětu.
     * 
     * @return způsob získání předmětu
     */
    public GetType getGetType()
    {
        return getType;
    }
}
