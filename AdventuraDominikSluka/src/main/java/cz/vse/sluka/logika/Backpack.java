package cz.vse.sluka.logika;

import java.util.HashMap;
import java.util.Map;

/**
 * Třída představující batoh.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class Backpack
{
    private int backpackSize;
    private int buyCount;
    private Map<String, Item> backpack;

    /**
     * Implicitní konstruktor třídy.
     */
    public Backpack()
    {
        this.backpack = new HashMap<String, Item>();
        this.backpackSize = 0;
        this.buyCount = 0;
    }

    /**
     * Konstruktor třídy.
     *
     * @param backpackSize, velikst batohu
     */
    public Backpack(int backpackSize)
    {
        this.backpack = new HashMap<String, Item>();
        this.backpackSize = backpackSize;
        this.buyCount = 0;
    }

    /**
     * Metoda, která vypíše obsah batohu.
     * 
     * @return obsah batohu
     */
    public String getBackpackContent(){
        String content ="";
        if (backpack.isEmpty()) {
            return "Batoh je prázdný.";
        }
        content = content + backpack.keySet();
        return content;
    }

    public Map<String, Item> getBackpackContents(){
        return backpack;
    }

    /**
     * Metoda, která vrátí věc z batohu.
     * 
     * @param itemName, jméno předmětu
     * @return obsah batohu
     */
    public Item getItemFromBag(String itemName){
        if (backpack.isEmpty()) {
            return null;
        }
        return backpack.get(itemName);
    }

    /**
     * Metoda, která zkontroluje zda je věc v batohu.
     * 
     * @param itemName, jméno předmětu
     * @return {@code true}, pokud je věc v batohu; jinak {@code false}
     */
    public boolean isItemAtBag(String itemName){
        if (backpack.isEmpty()) {
            return false;
        }
        for(String item: backpack.keySet()){
            if (itemName.equals(item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda, která vloží věc do batohu. Pokud je v batohu ještě místo.
     * 
     * @param item, předmět který bude vložen do batohu
     * @return informaci o tom, da se předmět sebral a uložil do batohu a nebo neuložil, protože je batoh plný.
     */
    public String putInBackpack(Item item){
        if(backpack.size() < backpackSize){
            backpack.put(item.getName(), item);
            return "Sebral jste předmět '" + item.getName() + "' a uložil jste ho do inventáře.";
        }
        return "Batoh je plný.";
    }


    /**
     * Metoda odebere předmět z batohu. Pokud je předmět v batohu.
     * 
     * @param item, předmět který bude odebrán
     * @return odebraný předmět, pokud se v batohu objekt nachází; jinak informaci o tom, že v batohu předmět není.
     */
    public String takeOutOfBackpack(Item item){
        if (!backpack.containsKey(item.getName())) {
            return item.getName() + " v batohu není.";
        }

        backpack.remove(item.getName());

        return item.getName() + " byl odebrán z batohu.";
    }

    public boolean takeOutBackpack(String itemName)
    {
        if(isItemAtBag(itemName))
        {
            backpack.remove(itemName);
            return true;
        }
        else
        {
            return false;
        }
    }
    /**
     * Metoda, která vrací počet provedených nákupů.
     * 
     * @return počet nákupů
     */
    public int getBuyCount() {
        return buyCount; 
    }

    /**
     * Metoda, která zvýší počet provedených nákupů o 1.
     */
    public void increaseBuyCount() {
        buyCount ++;
    }

    /**
     * Metoda, která vynuluje počet provedených nákupů. Využívá se po jídle.
     */
    public void clearBuyCount() {
        buyCount = 0 ;
    }

}
