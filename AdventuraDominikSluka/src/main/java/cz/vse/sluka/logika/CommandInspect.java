package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro detailnější prozkoumání předmětu.
 * Předmět se musí nacházet v dané lokaci nebo v batohu.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandInspect implements ICommand
{
    private static final String NAME = "prozkoumej";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na hru, která má být příkazem ukončena
     */
    public CommandInspect(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí vypsat detailnější popis předmětu. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznámý předmět pro prozkoumání)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. nelze prozkoumat více předmětů najednou)</i>, vrátí chybové hlášení.
     * Pokud byl zadán právě jeden parametr,zkontroluje, zda se daný předmět nachází v aktuální lokaci nebo v batobu.
     * Pokud ne, vrátí chybové hlášení. Pokud všechny kontroly proběhnou v pořádku, vypíše popis předmětu.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám prozkoumat, musíte zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím prozkoumat více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (area.containsItem(itemName)) {
            return area.getItem(itemName).getDescription();
        }

        if (plan.getBackpack().isItemAtBag(itemName)) {
            return plan.getBackpack().getItemFromBag(itemName).getDescription();
        }

        return "Předmět '" + itemName + "' tady není.";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    public String getName()
    {
        return NAME;
    }
}
