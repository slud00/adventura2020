package cz.vse.sluka.logika;

/**
 * Třída představující aktuální stav hry. Veškeré informace o stavu hry
 * <i>(mapa lokací, inventář, vlastnosti hlavní postavy, informace o plnění
 * úkolů apod.)</i> by měly být uložené zde v podobě datových atributů.
 * <p>
 * Třída existuje především pro usnadnění potenciální implementace ukládání
 * a načítání hry. Pro uložení rozehrané hry do souboru by mělo stačit uložit
 * údaje z objektu této třídy <i>(např. pomocí serializace objektu)</i>. Pro
 * načtení uložené hry ze souboru by mělo stačit vytvořit objekt této třídy
 * a vhodným způsobem ho předat instanci třídy {@link Game}.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 *
 * @see <a href="https://java.vse.cz/4it101/AdvSoubory">Postup pro implementaci ukládání a načítání hry na předmětové wiki</a>
 * @see java.io.Serializable
 */
public class GamePlan
{
    private static final String FINAL_LOCATION_NAME = "domov";

    private Area currentArea;
    private Area exitArea;
    private Backpack backpack;

    /**
     * Konstruktor třídy. Pomocí metody {@link #prepareWorldMap() prepareWorldMap}
     * vytvoří jednotlivé lokace a propojí je pomocí východů.
     */
    public GamePlan()
    {
        this.backpack = new Backpack(4);
        prepareWorldMap();
    }

    /**
     * Metoda vytváří jednotlivé lokace a propojuje je pomocí východů. Jako
     * výchozí aktuální lokaci následně nastaví domov.
     */
    private void prepareWorldMap()
    {
        Area domov = new Area(FINAL_LOCATION_NAME,"Pokud již máte všechny věci ze seznamu, vyložte je a vyhrajete.\n" + 
                "Pokud všechny nemáte, jděte je sehnat. Aktuálně máte možnost jít do garáže. ", AreaType.HOME, false);
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domů.\n" + 
                "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);
        Area parkoviste = new Area("parkoviště","Pokud již máte všechny věci koupené, jeďte do garáže.\n" +  
                "Pokud je všechny nemáte, jděte je koupit do obchodu, ale pouze pokud máte peníze. Ty popřípadě jděte vybrat do banky. ", AreaType.PARKING,true);
        Area banka= new Area("banka","Pokud nemáte peníze, přistupte k bankomatu a vezměte peníze. Pokud peníze máte, vraťte se na parkoviště. ", AreaType.BANK, false);
        Area obchodniCentrum = new Area("obchodní_centrum","Pokud máte všechny věci, vraťte se na parkoviště. Jinak jděte nakupovat. Pokud máte hlad, jděte do restaurace. ", AreaType.OTHER, false);
        Area restaurace = new Area("restaurace","Pokud máte hlad, najezte se. V opačném případě se vraťte do obchodu. ", AreaType.RESTAURANT,false);
        Area drogerie = new Area("drogerie","Pokud již máte koupené mýdlo, vraťte se do obchodu. V opačném případě si vezměte mýdlo. ", AreaType.SHOP, false);
        Area lekarna = new Area("lékárna","Pokud již máte koupený brufen, vraťte se do obchodu. V opačném případě si vezměte brufen. ", AreaType.SHOP, false);
        Area potraviny = new Area("potraviny","Pokud již máte koupený chléb, vraťte se do obchodu. V opačném případě si vezměte chléb. ", AreaType.SHOP, false);

        domov.addExit(garaz);
        garaz.addExit(domov);

        garaz.addExit(parkoviste);
        parkoviste.addExit(garaz);

        parkoviste.addExit(banka);
        banka.addExit(parkoviste);

        parkoviste.addExit(obchodniCentrum);
        obchodniCentrum.addExit(parkoviste);

        obchodniCentrum.addExit(restaurace);
        restaurace.addExit(obchodniCentrum);

        obchodniCentrum.addExit(drogerie);
        drogerie.addExit(obchodniCentrum);

        obchodniCentrum.addExit(lekarna);
        lekarna.addExit(obchodniCentrum);

        obchodniCentrum.addExit(potraviny);
        potraviny.addExit(obchodniCentrum);

        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item penize = new Item("peníze", "Peníze, které lze použít pro nákup věcí v obchodech.", GetType.TAKEOUT, true);
        Item mydlo = new Item("mýdlo", "Mýdlo, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item brufen = new Item("brufen", "Brufen, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item chleb = new Item("chléb", "Chléb, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item auto = new Item("auto", "Auto, pro přesunování mezi parkovištěm a garáží.", GetType.TAKE, false);
        Item bankomat = new Item("bankomat", "Bankomat pro výběr peněz.", GetType.OTHER, false);

        garaz.addItem(klice);
        garaz.addItem(auto);
        parkoviste.addItem(auto);
        banka.addItem(penize);
        banka.addItem(bankomat);
        drogerie.addItem(mydlo);
        lekarna.addItem(brufen);
        potraviny.addItem(chleb);

        currentArea = domov;
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea()
    {
        return currentArea;
    }

    
    /**
     * Metoda nastaví aktuální lokaci, používá ji příkaz {@link CommandMove}
     * při přechodu mezi lokacemi.
     *
     * @param area lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area area)
    {
        currentArea = area;
    }

    /**
     * Metoda pro zjištění, zda hráč hru vyhrál. Tudíž pokud se nachází všechny předměty ze seznamu (mýdlo, chléb, brufen) v lokaci domov.
     *
     * @param domov lokace, ve které je potřeba se nacházet pro vyhrání hry.
     * @return {@code true}, pokud se všechny předměty ze seznamu (mýdlo, chléb, brufen) nacházejí doma; jinak {@code false}
     */
    public boolean isVictorious(Area domov)
    {
        if (domov.containsItem("mýdlo") && domov.containsItem("chléb") && domov.containsItem("brufen")) {
            return true;
        }
        return false;
    }

    /** 
     * Metoda která vrací batoh.
     * 
     * @return objekt backpack
     */
    public Backpack getBackpack() {
        return backpack;
    }

}
