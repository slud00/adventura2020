package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro rozhlédnutí se (vypsání všech věcí v místnosti a všech možných východů).
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandLookAround implements ICommand
{
    private static final String NAME = "rozhlédni_se";

    private GamePlan plan;

    public CommandLookAround(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * 
     * Metoda vypíše předměty v místnosti a výstupy. Předtím však zkontroluje správnost vstupu.
     * 
     * @param parameters parametry příkazu <i>(očekává se prázdné pole)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length != 0) {
            return "Nevím, co to znamená.";
        }

        return plan.getCurrentArea().getDescription();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */@Override
    public String getName()
    {
        return NAME;
    }
}
