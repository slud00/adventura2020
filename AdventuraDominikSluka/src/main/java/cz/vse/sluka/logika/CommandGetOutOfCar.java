package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro vystoupení z auta.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandGetOutOfCar implements ICommand
{
    private static final String NAME = "vystup";

    private GamePlan plan;

    public CommandGetOutOfCar(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí vystoupit z auta. Zkontroluje správnost vstupu, zda je zadán pouze jeden parametr a zda se rovná autu. 
     * Jinak vypíše chybovou hlášku. Dále také kontroluje zda je hráč v autě.
     * Pokud vše projde, hráč vystoupí z auta a vypíše se hláška pro hráče. 
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám dělat.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím dělat více věcí najednou.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!"auto".equals(itemName)) {
            return "Z '" + itemName + "' nelze vystoupit.";
        }

        if (!area.getInCar()) {
            return "Nejste v autě.";
        }

        area.setInCar(false);
        return "Vystoupil jste z auta.";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */@Override
    public String getName()
    {
        return NAME;
    }
}
