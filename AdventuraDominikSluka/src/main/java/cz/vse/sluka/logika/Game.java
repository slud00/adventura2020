package cz.vse.sluka.logika;

/**
 * Hlavní třída logiky aplikace. Třída vytváří instanci třídy {@link GamePlan},
 * která inicializuje lokace hry, a vytváří seznam platných příkazů a instance
 * tříd provádějících jednotlivé příkazy.
 *
 * Během hry třída vypisuje uvítací a ukončovací texty a vyhodnocuje jednotlivé
 * příkazy zadané uživatelem.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @version LS 2020
 */
public class Game implements IGame
{
    private ListOfCommands listOfCommands;
    private GamePlan gamePlan;
    private boolean gameOver;

    /**
     * Konstruktor třídy. Vytvoří hru, inicializuje herní plán udržující
     * aktuální stav hry a seznam platných příkazů.
     */
    public Game()
    {
        gameOver = false;
        gamePlan = new GamePlan();
        listOfCommands = new ListOfCommands();

        listOfCommands.addCommand(new CommandHelp(listOfCommands));
        listOfCommands.addCommand(new CommandTerminate(this));
        listOfCommands.addCommand(new CommandMove(gamePlan));
        listOfCommands.addCommand(new CommandPick(gamePlan));
        listOfCommands.addCommand(new CommandInspect(gamePlan));
        listOfCommands.addCommand(new CommandRide(gamePlan));
        listOfCommands.addCommand(new CommandGetInTheCar(gamePlan));
        listOfCommands.addCommand(new CommandGetOutOfCar(gamePlan));
        listOfCommands.addCommand(new CommandPut(gamePlan));
        listOfCommands.addCommand(new CommandGoToATM(gamePlan));
        listOfCommands.addCommand(new CommandEat(gamePlan));
        listOfCommands.addCommand(new CommandBackpack(gamePlan));
        listOfCommands.addCommand(new CommandLookAround(gamePlan));

    }

    /**
     * Metoda, která na začátku hry vypíše prolog s pár informacemi.
     * 
     * @return text prologu
     */
    @Override
    public String getPrologue()
    {
        return "Vítejte!\n"
        + "Toto je interaktivní hra vytvořená jako semestrální práce pro předmět 4IT101.\n"
        + "Vašim úkolem je sesbírat nakoupit všechny věci na sezmanu (chléb, mýdlo, brufen ) a donést je domů.\n"
        + "Napište 'napoveda', pokud si nevíte rady, jak hrát dál.\n"
        + "\n"
        + gamePlan.getCurrentArea().getFullDescription();
    }

    /**
     * Metoda, která na konci hry vypíše epilog. Pokud hráč ukončí hru příkazem, poděkuje za hru.
     * Pokud hráč ukončí hru výhrou, vypíše že vyhrál.
     * 
     * @return text epilogu
     */
    @Override
    public String getEpilogue()
    {
        String epilogue = "Děkuju vám, že jste si zahrál.";

        if (gamePlan.isVictorious(gamePlan.getCurrentArea())) {
            epilogue = "ZVÍTĚZIL JSTE !\n\n" + epilogue;
        }

        return epilogue;
    }

    /**
     * Metoda, která kontroluje zda je hra ukončena.
     * 
     * @return informace zda je hra ukončena
     */
    @Override
    public boolean isGameOver()
    {
        return gameOver;
    }

    /**
     * Metoda, která z řetězce zpracovává jednotlivé příkazy.
     * 
     * @param line vstupní řetězec
     * @return výsledek zpracovaný příkazem
     */
    @Override
    public String processCommand(String line)
    {
        String[] words = line.split("[ \t]+");

        String cmdName = words[0];
        String[] cmdParameters = new String[words.length - 1];


        for (int i = 0; i < cmdParameters.length; i++) {
            cmdParameters[i] = words[i + 1];
        }

        String result = null;
        if (listOfCommands.checkCommand(cmdName)) {
            ICommand command = listOfCommands.getCommand(cmdName);
            result = command.process(cmdParameters);
        } else {
            result = "Nechápu, co po mně chcete. Tento příkaz neznám.";
        }

        if (gamePlan.isVictorious(gamePlan.getCurrentArea())) {
            gameOver = true;
        }

        return result;
    }

    /**
     * Třída, která vrací objekt gamePlan.
     * 
     * @return gamePlan
     */
    @Override
    public GamePlan getGamePlan()
    {
        return gamePlan;
    }

    /**
     * Metoda nastaví příznak indikující, že nastal konec hry. Metodu
     * využívá třída {@link CommandTerminate}, mohou ji ale použít
     * i další implementace rozhraní {@link ICommand}.
     *
     * @param gameOver příznak indikující, zda hra již skončila
     */
    void setGameOver(boolean gameOver)
    {
        this.gameOver = gameOver;
    }

}
