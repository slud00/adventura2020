package cz.vse.sluka.logika;

import java.util.*;

/**
 * Třída představuje lokaci <i>(místo, místnost, prostor)</i> ve scénáři hry.
 * Každá lokace má název, který ji jednoznačně identifikuje. Lokace může mít
 * sousední lokace, do kterých z ní lze odejít. Odkazy na všechny sousední
 * lokace jsou uložené v kolekci.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class Area
{
    public String getFullDescription;
    private String name;
    private String description;
    private Set<Area> exits;
    private Map<String, Item> items;
    private boolean byCar;
    private boolean inCar;
    private boolean nextByATM;
    private AreaType areaType;

    /**
     * Konstruktor třídy. Vytvoří lokaci se zadaným názvem, popisem, typem lokace a informací zda se lze z nebo do lokace pohybovat autem.
     *
     * @param name název lokace <i>(jednoznačný identifikátor, musí se jednat o text bez mezer)</i>
     * @param description podrobnější popis lokace
     * @param areaType typ místa
     * @param byCar možnost cesty autem
     */
    public Area(String name, String description, AreaType areaType, boolean byCar)
    {
        this.name = name;
        this.description = description;
        this.areaType = areaType;
        this.exits = new HashSet<>();
        this.items = new HashMap<>();
        this.byCar = byCar;  
        this.inCar = false;  
        this.nextByATM = false;
    }

    /**
     * Metoda vrací název lokace, který byl zadán při vytváření instance jako
     * parametr konstruktoru. Jedná se o jednoznačný identifikátor lokace <i>(ve
     * hře nemůže existovat více lokací se stejným názvem)</i>. Aby hra správně
     * fungovala, název lokace nesmí obsahovat mezery, v případě potřeby můžete
     * více slov oddělit pomlčkami, použít camel-case apod.
     *
     * @return název lokace
     */
    public String getName()
    {
        return name;
    }

    /**
     * Metoda přidá další východ z této lokace do lokace předané v parametru.
     * <p>
     * Vzhledem k tomu, že pro uložení sousedních lokací se používá {@linkplain Set},
     * může být přidán pouze jeden východ do každé lokace <i>(tzn. nelze mít dvoje
     * 'dveře' do stejné sousední lokace)</i>. Druhé volání metody se stejnou lokací
     * proto nebude mít žádný efekt.
     * <p>
     * Lze zadat též cestu do sebe sama.
     *
     * @param area lokace, do které bude vytvořen východ z aktuální lokace
     */
    public void addExit(Area area)
    {
        exits.add(area);
    }

    /**
     * Metoda vrací detailní informace o lokaci. Výsledek volání obsahuje název
     * lokace, podrobnější popis, seznam sousedních lokací, do kterých lze
     * odejít a předměty v místnosti.
     *
     * @return detailní informace o lokaci
     */
    public String getFullDescription()
    {
        String exitNames = "Východy:";
        for (Area exitArea : exits) {
            exitNames += " " + exitArea.getName();
        }

        String itemNames = "Předměty:";
        for (String itemName : items.keySet()) {
            itemNames += " " + itemName;
        }

        return "Jste v lokaci (místnosti) " + name + ".\n"
        + description + "\n\n"
        + exitNames + "\n"
        + itemNames;
    }

    /**
     * Metoda vrací informace o lokaci. Výsledek volání obsahuje název
     * lokace, seznam věcí v lokaci a seznam sousedních lokací, do kterých lze
     * odejít a předměty v místnosti.
     *
     * @return detailní informace o lokaci
     */
    public String getDescription()
    {
        String exitNames = "Východy:";
        for (Area exitArea : exits) {
            exitNames += " " + exitArea.getName();
        }

        String itemNames = "Předměty:";
        for (String itemName : items.keySet()) {
            itemNames += " " + itemName;
        }

        return "Jste v lokaci (místnosti) " + name + ".\n"
        + exitNames + "\n"
        + itemNames;
    }

    public String getDescriptions()
    {
        return description;
    }
    /**
     * Metoda vrací lokaci, která sousedí s aktuální lokací a jejíž název
     * je předán v parametru. Pokud lokace s daným jménem nesousedí s aktuální
     * lokací, vrací se hodnota {@code null}.
     * <p>
     * Metoda je implementována pomocí tzv. 'lambda výrazu'. Pokud bychom chtěli
     * metodu implementovat klasickým způsobem, kód by mohl vypadat např. tímto
     * způsobem:
     * <pre> for (Area exitArea : exits) {
     *     if (exitArea.getName().equals(areaName)) {
     *          return exitArea;
     *     }
     * }
     *
     * return null;</pre>
     *
     * @param areaName jméno sousední lokace <i>(východu)</i>
     * @return lokace, která se nachází za příslušným východem; {@code null}, pokud aktuální lokace s touto nesousedí
     */
    public Area getExitArea(String areaName)
    {
        return exits.stream()
        .filter(exit -> exit.getName().equals(areaName))
        .findAny().orElse(null);
    }

    public Collection<Area> getExits(){
        return Collections.unmodifiableCollection(exits);
    }


    /**
     * Metoda porovnává dvě lokace <i>(objekty)</i>. Lokace jsou shodné,
     * pokud mají stejný název <i>(atribut {@link #name})</i>. Tato metoda
     * je důležitá pro správné fungování seznamu východů do sousedních
     * lokací.
     * <p>
     * Podrobnější popis metody najdete v dokumentaci třídy {@linkplain Object}.
     *
     * @param o objekt, který bude porovnán s aktuálním
     * @return {@code true}, pokud mají obě lokace stejný název; jinak {@code false}
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (!(o instanceof Area)) {
            return false;
        }

        Area area = (Area) o;

        return Objects.equals(this.name, area.name);
    }

    /**
     * Metoda vrací číselný identifikátor instance, který se používá
     * pro optimalizaci ukládání v dynamických datových strukturách
     * <i>(např.&nbsp;{@linkplain HashSet})</i>. Při překrytí metody
     * {@link #equals(Object) equals} je vždy nutné překrýt i tuto
     * metodu.
     * <p>
     * Podrobnější popis pravidel pro implementaci metody najdete
     * v dokumentaci třídy {@linkplain Object}.
     *
     * @return číselný identifikátor instance
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(name);
    }

    /**
     * Metoda přidá předmět <i>(objekt třídy {@link Item})</i> do lokace.
     * 
     * @param item předmět, který bude do lokace přidán
     */
    public void addItem(Item item)
    {
        items.put(item.getName(), item);
    }

    /**
     * Metoda odebere předmět <i>(objekt třídy {@link Item})</i> z lokace.
     * 
     * @param itemName předmětu, který bude z lokace odebrán
     * @return objekt odebraného předmětu
     */
    public Item removeItem(String itemName)
    {
        return items.remove(itemName);
    }

    /**
     * Metoda vrátí jméno předmětu
     * 
     * @param itemName předmětu
     * @return jméno předmětu
     */
    public Item getItem(String itemName)
    {
        return items.get(itemName);
    }

    /**
     * Metoda zjistí, zda se předmět nachází v místnosti.
     * 
     * @param itemName předmětu, o kterém se zjistí zda je v místnosti
     * @return {@code true}, pokud se v místnosti objekt nachází; jinak {@code false}
     */
    public boolean containsItem(String itemName)
    {
        return items.containsKey(itemName);
    }

    /**
     * Metoda vrací zda se z lokace lze pohybovat autem.
     * 
     * @return {@code true}, pokud se z a do lokace dá pohybovat autem; jinak {@code false}
     */
    public boolean getByCar() {
        return byCar;
    }

    /**
     * Metoda vrací zda je hráč v autě.
     * 
     * @return {@code true}, pokud je v autě; jinak {@code false}
     */
    public boolean getInCar() {
        return  inCar;
    }

    /**
     * Metoda (setter) zajišťující nástup/výstup z auta.
     * 
     * @param inCar, který indikuje zda jste nebo nejste v autě.
     */
    public void setInCar(boolean inCar) {
        this.inCar = inCar;
    }

    /**
     * Metoda vrací zda je hráč u bankomatu.
     * 
     * @return {@code true}, jste u bankomatu; jinak {@code false}
     */
    public boolean getNextByATM() {
        return nextByATM;
    }

    /**
     * Metoda nastaví přítomnost u bankomatu.
     * 
     * @param nextByATM, který indikuje zda je v místnosti bankomat.
     */
    public void setNextByATM(boolean nextByATM) {
        this.nextByATM = nextByATM;
    }

    /**
     * Metoda vrací typ místa.
     * 
     * @return typ místonsti
     */
    public AreaType getAreaType() {
        return areaType;
    }

    /**
     * Vrací seznam předmětů v místnosti.
     * 
     * @return předměty v místnosti
     */
    public Map<String, Item> getItems() {
        return items;
    }


}
