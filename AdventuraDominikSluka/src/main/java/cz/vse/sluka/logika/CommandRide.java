package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro jízdu autem mezi herními lokacemi garáž a parkoviště.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandRide implements ICommand
{
    private static final String NAME = "jeď";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandRide(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí přesunout hráče do jiné lokace po tom co nastoupil do auta. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznáme cíl cesty)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. hráč chce jet na více míst
     * současně)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda je hráč v autě a zda s aktuální lokací sousedí jiná lokace s daným názvem, do které lze jet autem <i>(tj.
     * zda z aktuální lokace lze jet do požadovaného cíle)</i>. Pokud ne, vrátí chybové hlášení.
     * Pokud všechny kontroly proběhnou v pořádku, změní informaci o nastoupení do auta provede přesun
     * hráče i předmětů naložených v autě do cílové lokace, kde nastaví, že sedíte v autě a vrátí její popis.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nechápu, kam mám jet. Musíte mi zadat nějaký cíl.";
        } else if (parameters.length > 1) {
            return "Nechápu, co po mě chcete. Neumím se 'rozdvojit' a jet na více míst současně.";
        }

        String exitName = parameters[0];

        Area exitArea = plan.getCurrentArea().getExitArea(exitName);

        if (!plan.getCurrentArea().getInCar()) {
            return "Nejste v autě.";
        }

        if (exitArea == null || (!plan.getCurrentArea().getExitArea(exitName).getByCar()) || !plan.getCurrentArea().getByCar()) {
            return "Tam se ale odsud jet nedá.";
        }

        plan.getCurrentArea().setInCar(false);
        plan.getCurrentArea().getExitArea(exitName).setInCar(true);
 
        Item item = plan.getCurrentArea().getItem("auto");
        plan.getCurrentArea().removeItem("auto");

        exitArea.addItem(item);

        if (AreaType.PARKING.equals(plan.getCurrentArea().getAreaType()) 
        && AreaType.GARAGE.equals(plan.getCurrentArea().getExitArea(exitName).getAreaType())){

            for (String itemNam: plan.getCurrentArea().getItems().keySet()) {
                Item ite = plan.getCurrentArea().getItem(itemNam);
                if (ite.isMoveable()) {
                    plan.getCurrentArea().getExitArea(exitName).addItem(ite);
                    plan.getCurrentArea().removeItem(itemNam);
                }
            }
        }

        plan.setCurrentArea(exitArea); 

        return exitArea.getFullDescription();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}

