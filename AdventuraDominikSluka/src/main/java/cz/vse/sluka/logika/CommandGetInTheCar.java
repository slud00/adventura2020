package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro nastoupení do auta.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandGetInTheCar implements ICommand
{
    private static final String NAME = "nastup";

    private GamePlan plan;

    public CommandGetInTheCar(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí nastoupit do auta. Nejprve zkontroluje správnost vstupu. Zda je zadán pouze jeden parametr a je roven "auto". 
     * Dále zkontroluje zda se auto nachází v místnosti a zda má hráč klíče od auta, které jsou nutné pro nastoupení. 
     * Pokud hráč již je v autě, vypíše se chybová hláška.
     * Pokud vše projde, hráč nastoupí do auta a vypíše se hláška pro hráče.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám dělat.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím dělat více věcí najednou";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!"auto".equals(itemName)) {
            return "Do '" + itemName + "' nelze nastoupit.";
        }

        if (!area.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }

        if (!plan.getBackpack().isItemAtBag("klíče")) {
            return "Nemáte klíče.";
        }

        if (area.getInCar()) {
            return "Už jste v autě.";
        }

        area.setInCar(true);
        return "Nastoupil jste do auta.";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */@Override
    public String getName()
    {
        return NAME;
    }
}
