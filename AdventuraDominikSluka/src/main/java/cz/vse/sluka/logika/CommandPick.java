package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro sebrání předmětu.
 * Kontroluje počet vstupů (akceptuje pouze jeden vstup), zda se předmět nachází v aktuální lokaci, zda je předmět přenositelný a jestli se předmět vejde do batohu.
 * 
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandPick implements ICommand
{
    private static final String NAME = "vezmi";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandPick(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí sebrat předmět. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznámý předmět pro sebrání)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. nelze sebrat více předmětů najednou)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda se daný předmět nachází v aktuální lokaci. Pokud ne, vrátí
     * chybové hlášení. Dále zkontroluje zda je předmět přenositelný. Dále zkontroluje zda jste v bance u bankomatu pro sebrání peněz v bance. Pokud ne, vrátí
     * chybové hlášení. Dále zkontroluje zda máte peníze v batohu pro sebrání produktů v obchodech. 
     * Dále zkontroluje zda neuběhli dva nákupy od najezení se.  V poslední řadě zkontroluje zda se předmět vejde do batohu.
     * Pokud všechny kontroly proběhnou v pořádku, sebere předmět.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám sebrat, musíte zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím sebrat více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!area.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }

        Item item = area.getItem(itemName);

        if (!item.isMoveable()) {
            return "Předmět '" + itemName + "' fakt neuneseš.";
        }

        if (GetType.TAKEOUT.equals(item.getGetType()) && !area.getNextByATM() && AreaType.BANK.equals(area.getAreaType())) {  
            return "Nejste u bankomatu. Nemůžete vybrat " + itemName +".";
        }

        if (!plan.getBackpack().isItemAtBag("peníze") && GetType.BUY.equals(item.getGetType()) && AreaType.SHOP.equals(area.getAreaType())) {  
            return "Nemáte peníze.";
        }

        if (plan.getBackpack().getBuyCount() >= 2 && GetType.BUY.equals(item.getGetType()) &&  AreaType.SHOP.equals(area.getAreaType())) {  
            return "Jste hladový. Jdi se najíst.";
        } 

        String result = plan.getBackpack().putInBackpack(item);

        if (!"Batoh je plný.".equals(result)) { 
            area.removeItem(itemName);
            if (GetType.BUY.equals(item.getGetType()) &&  AreaType.SHOP.equals(area.getAreaType())) {
                plan.getBackpack().increaseBuyCount();
            }
        }

        return result;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
