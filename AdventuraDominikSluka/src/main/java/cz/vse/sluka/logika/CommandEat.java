package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro najezení se v restauraci.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandEat implements ICommand
{
    private static final String NAME = "najez_se";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandEat(GamePlan plan) 
    {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí, aby se hráč najedl. Zkontroluje vstup a následně i to, zda se hráč nachází v restauraci. 
     * Pokud se vše podaří, vynuluje počet nákupů od posledního jídla.
     *
     * @param parameters parametr příkazu <i>(očekává se prázdné pole)</i>
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "Nechápu, co po mě chcete.";
        }

        if (!AreaType.RESTAURANT.equals(plan.getCurrentArea().getAreaType())) {
            return "Nejste v restauraci."; 
        }

        plan.getBackpack().clearBuyCount();

        return "Dobrou chuť.";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }
}
