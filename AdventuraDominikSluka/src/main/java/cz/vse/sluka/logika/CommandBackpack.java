package cz.vse.sluka.logika;

/**
 * Třída implementující příkaz pro vypsání všech věcí z batohu.
 *
 * @author Dominik Sluka
 * @version LS 2020
 */
public class CommandBackpack implements ICommand
{
    private static final String NAME = "batoh";

    private GamePlan plan;

    public CommandBackpack(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda vypíše obsah batohu pokud je příkaz zadán správně.
     *
     * @param parameters parametry příkazu <i>(očekává se prázdné pole)</i>
     * @return obsah batohu
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length != 0) {
            return "Nevím, co to znamená.";
        }

        Backpack backpack = plan.getBackpack();

        return backpack.getBackpackContent();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */@Override
    public String getName()
    {
        return NAME;
    }
}
