package cz.vse.sluka;

import cz.vse.sluka.logika.*;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * Třída MainController, která popisuje metody, které se vyvolávají ve hře.
 *
 * @author Dominik Sluka
 * @version ZS 2020
 */
public class MainController {

    public TextArea textOutput;
    public TextField textInput;
    public Label locationName;
    public Label locationDescription;
    public ImageView background;
    private IGame game;
    private Stage primaryStage;
    public VBox exits;
    public VBox items;
    public VBox backpack;
    public VBox ATMOptions;
    public VBox carOptions;
    public VBox eatOptions;
    public VBox helpOptions;

    /**
     * Metoda, která nastavuje základní objekty při spuštění hry.
     * @param game hra
     * @param primaryStage okno
     */
    public void init(IGame game, Stage primaryStage) {
        this.game = game;
        this.primaryStage = primaryStage;
        update();
    }

    /**
     * Metoda pro aktualizaci hry. Nastaví jméno, popis lokace a příslušné pozadí.
     * Dále aktualizuje veškeré východy, věci v lokaci, věci v batohu, možnosti v lokaci a zkontroluje, zda hra neskončila.
     */
    private void update() {
        String location = getCurrentArea().getName();
        locationName.setText(location);
        String description = getCurrentArea().getDescriptions();
        locationDescription.setText(description);

        InputStream stream = getClass().getClassLoader().getResourceAsStream(location + "1.jpg");
        Image img = new Image(stream);
        background.setImage(img);
        background.setFitHeight(1090);
        background.setFitWidth(1600);

        updateExits();
        updateItems();
        updateBackpack();
        updateCar();
        updateATM();
        updateEat();
        updateHelp();
        checkEnd();
    }

    /**
     * Metoda, která do hry přidává graficky možnost zobrazení nápovědy, ve kterékoliv lokaci.
     */
    private void updateHelp() {
        helpOptions.getChildren().clear();
        String Help = "Nápověda ke hře";
        Label helpLabel = new Label(Help);
        helpLabel.setCursor(Cursor.HAND);
        helpLabel.getStyleClass().add("Zatmaveni2");
        helpLabel.setOnMouseClicked(event -> {
            String result = game.processCommand("nápověda");
            textOutput.appendText(result + "\n \n");
            update();
        });
        helpOptions.getChildren().add(helpLabel);
    }

    /**
     * Metoda, která do hry přidává graficky možnost najezení se. Avšak pouze v lokaci restaurace.
     */
    private void updateEat() {
        eatOptions.getChildren().clear();
        if ("restaurace".equals(getCurrentArea().getName())) {
            String LetsEat = "Najez se";
            Label eatLabel = new Label(LetsEat);
            eatLabel.setCursor(Cursor.HAND);
            eatLabel.getStyleClass().add("Zatmaveni2");
            eatLabel.setOnMouseClicked(event -> {
                String result = game.processCommand("najez_se");
                textOutput.appendText(result + "\n \n");
                update();
            });
            eatOptions.getChildren().add(eatLabel);
        } else {
            eatOptions.getChildren().clear();
        }
    }

    /**
     * Metoda, která do hry přidává graficky možnost přistoupit k bankomatu. Avšak pouze v lokaci banka.
     */
    private void updateATM() {
        ATMOptions.getChildren().clear();
        if ("banka".equals(getCurrentArea().getName()) && !(getCurrentArea().getNextByATM())) {
            String GoToATM = "Přistup k bankomatu";
            Label ATMLabel = new Label(GoToATM);
            ATMLabel.setCursor(Cursor.HAND);
            ATMLabel.getStyleClass().add("Zatmaveni2");
            ATMLabel.setOnMouseClicked(event -> {
                String result = game.processCommand("přistup bankomat");
                textOutput.appendText(result + "\n \n");
                update();
            });
            ATMOptions.getChildren().add(ATMLabel);
        } else {
            ATMOptions.getChildren().clear();
        }
    }

    /**
     * Metoda, která do hry přidává graficky možnost nastoupit do auta a vystoupit z auta.
     * Avšak pouze v lokaci, do které se dá jet autem.
     */
    private void updateCar() {
        carOptions.getChildren().clear();
        if (getCurrentArea().getByCar() && !getCurrentArea().getInCar()) {
            String getInCar = "Nastup do auta";
            Label carLabel = new Label(getInCar);
            carLabel.setCursor(Cursor.HAND);
            carLabel.getStyleClass().add("Zatmaveni2");
            carLabel.setOnMouseClicked(event -> {
                String result = game.processCommand("nastup auto");
                textOutput.appendText(result + "\n \n");
                update();
            });
            carOptions.getChildren().add(carLabel);
        }
        if (getCurrentArea().getByCar() && getCurrentArea().getInCar()) {
            String getInCar = "Vystup z auta";
            Label carLabel = new Label(getInCar);
            carLabel.setCursor(Cursor.HAND);
            carLabel.getStyleClass().add("Zatmaveni2");
            carLabel.setOnMouseClicked(event -> {
                String result = game.processCommand("vystup auto");
                textOutput.appendText(result + "\n \n");
                update();
            });
            carOptions.getChildren().add(carLabel);
        }
    }

    /**
     * Metoda pro batoh a pokládání věcí z batohu zpět do lokace. Dále metoda k příslušným předmětům v batohu přiřazuje i obrázky.
     */
    private void updateBackpack() {
        Collection<Item> backpackList = getBackpackContent().getBackpackContents().values();
        backpack.getChildren().clear();

        for (Item backpackItem : backpackList) {
            String backpackItemName = backpackItem.getName();
            Label backpackItemLabel = new Label(backpackItemName);
            backpackItemLabel.setCursor(Cursor.HAND);
            backpackItemLabel.setTooltip(new Tooltip(backpackItem.getDescription()));
            backpackItemLabel.getStyleClass().add("Zatmaveni");

            setPicture(backpackItemName, backpackItemLabel);

            backpackItemLabel.setOnMouseClicked(event -> {
                String result = game.processCommand("polož " + backpackItemName);
                game.getGamePlan().getBackpack().takeOutBackpack(backpackItemName);
                textOutput.appendText(result + "\n\n");
                update();
            });
            backpack.getChildren().add(backpackItemLabel);
        }
    }

    /**
     * Metoda pro zjednodušení opakujících se příkazů pro nastavení obrázku k věci.
     *
     * @param backpackItemName název předmětu, pro který se má nastavit obrázek
     * @param backpackItemLabel označení předmětu, pro který se má nastavit obrázek
     */
    private void setPicture(String backpackItemName, Label backpackItemLabel) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(backpackItemName + ".jpg");
        Image img = new Image(stream);
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(85);
        imageView.setFitHeight(60);
        backpackItemLabel.setGraphic(imageView);
    }

    /**
     * Metoda pro předměty a jejich sebrání do batohu. Dále metoda k příslušným předmětům v lokaci přiřazuje i obrázky.
     */
    private void updateItems() {
        Collection<Item> itemList = getCurrentArea().getItems().values();
        items.getChildren().clear();

        for (Item item : itemList) {
            String itemName = item.getName();
            Label itemLabel = new Label(itemName);
            itemLabel.setCursor(Cursor.HAND);
            itemLabel.setTooltip(new Tooltip(item.getDescription()));
            itemLabel.getStyleClass().add("Zatmaveni");

            setPicture(itemName, itemLabel);

            itemLabel.setOnMouseClicked(event -> {
                executeCommand("vezmi " + itemName);
            });
            items.getChildren().add(itemLabel);
        }
    }

    /**
     *Metoda, která zpracovává příkazy pro grafiku. Dále nastavuje do outputu text.
     *
     * @param command příkaz
     */
    private void executeCommand(String command) {
        String result = game.processCommand(command);

        textOutput.appendText(result + "\n \n");
        update();
    }

    /**
     * Metoda pro zobrazení východů z aktuální lokace.
     * Metoda také podle podmínky vybírá, zda se mezi lokacemi má použít příkaz "jdi" nebo "jeď".
     */
    private void updateExits() {
        Collection<Area> exitList = getCurrentArea().getExits();
        exits.getChildren().clear();

        for (Area area : exitList) {
            String exitName = area.getName();
            Label exitLabel = new Label(exitName);

            exitLabel.setCursor(Cursor.HAND);
            exitLabel.setTooltip(new Tooltip(area.getDescriptions()));
            exitLabel.getStyleClass().add("Zatmaveni");

            setPicture(exitName, exitLabel);

            exitLabel.setOnMouseClicked(event -> {
                if (!getCurrentArea().getExitArea(exitName).getByCar() || !getCurrentArea().getByCar()) {
                    executeCommand("jdi " + exitName);
                } else {
                    executeCommand("jeď " + exitName);
                }
            });

            exits.getChildren().add(exitLabel);
        }
    }

    /**
     * Metoda, která zobrazí podrobné informace o hře z html souboru v novém okně.
     */
    public void aboutTheGame() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("/Informace.html").toString());
        Scene scene = new Scene(webView, 600, 600);
        secondStage.setScene(scene);
        secondStage.setTitle("Informace");
        secondStage.show();
    }

    /**
     * Metoda, která zobrazí mapu hry v novém okně.
     */
    public void map() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("/mapa.jpg").toString());
        Scene scene = new Scene(webView, 600, 600);
        secondStage.setScene(scene);
        secondStage.setTitle("Mapa");
        secondStage.show();
    }

    /**
     * Metoda, která po kliknutí spustí novou hru od začátku.
     */
    public void newGame() {

        primaryStage.setTitle("Game");
        FXMLLoader loader = new FXMLLoader();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");
        Parent root = null;
        try {
            root = loader.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(true);

        MainController controller = loader.getController();
        IGame game = new Game();
        controller.init(game, primaryStage);
        primaryStage.show();
    }

    /**
     * Metoda, která po kliknutí ukončí hru.
     */
    public void endOfTheGame() {
        Platform.exit();
    }

    /**
     * Metoda, která kontroluje zda je hra u konce.
     * Pokud ano zablokuje psaní do inputu a zamezí veškerým operacím jako např. posouvání se mezi lokacemi nebo pohyb s věcmi.
     */
    public void checkEnd() {
        if (game.isGameOver()) {
            textInput.setEditable(false);
            game = null;
        }
    }

    /**
     * Metoda, která vrací aktuální lokaci z herního plánu.
     *
     * @return aktuální lokace
     */
    private Area getCurrentArea() {
        return game.getGamePlan().getCurrentArea();
    }

    /**
     * Metoda, která vrací obsah batohu.
     *
     * @return obsah batohu
     */
    private Backpack getBackpackContent() {
        return game.getGamePlan().getBackpack();
    }

    /**
     * Metoda, která zaznamenává stisklou klávesu v textInput.
     *
     * @param keyEvent označení stisku klávesy
     */
    public void onInputKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            executeCommand(textInput.getText());
            textInput.setText("");
        }
    }
}