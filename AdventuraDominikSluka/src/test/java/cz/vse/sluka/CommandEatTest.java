package cz.vse.sluka.test;

import cz.vse.sluka.logika.Area;
import cz.vse.sluka.logika.AreaType;
import cz.vse.sluka.logika.CommandEat;
import cz.vse.sluka.logika.Game;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro najezení se {@link CommandEat}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandEatTest
{
    private Game game;

    /**
     * Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandEatTestExecute()
    {
        Area restaurace = new Area("restaurace","Pokud máte hlad, najezte se. V opačném případě se vraťte do obchodu. ", AreaType.RESTAURANT,false);
        Area drogerie = new Area("drogerie","Pokud již máte koupené mýdlo, vraťte se do obchodu. V opačném případě si vezměte mýdlo. ", AreaType.SHOP, false);

        game.getGamePlan().setCurrentArea(drogerie);
        Assert.assertEquals("Nejste v restauraci.", game.processCommand("najez_se"));

        game.getGamePlan().setCurrentArea(restaurace);
        Assert.assertEquals("Dobrou chuť.", game.processCommand("najez_se"));

    }
}
