package cz.vse.sluka.test;

import cz.vse.sluka.logika.Area;
import cz.vse.sluka.logika.AreaType;
import cz.vse.sluka.logika.CommandMove;
import cz.vse.sluka.logika.Game;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro třídu zajišťující pohyb {@link CommandMove}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandMoveTest
{
    private Game game;

    /**
     *  Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandRideTestExecute()
    {
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domu.\n" +
                "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);
        Area parkoviste = new Area("parkoviště","Pokud již máte všechny věci koupené, jeďte do garáže.\n" +  
                "Pokud je všechny nemáte, jděte je koupit do obchodu, ale pouze pokud máte peníze. Ty popřípadě jděte vybrat do banky. ", 
                AreaType.PARKING,true);
        Area obchodniCentrum = new Area("obchodní_centrum","Pokud máte všechny věci, vraťte se na parkoviště. Jinak jděte nakupovat. Pokud máte hlad, jděte do restaurace. ",
                AreaType.OTHER, false);

        garaz.addExit(parkoviste);
        parkoviste.addExit(garaz);
        parkoviste.addExit(obchodniCentrum);
        garaz.addExit(obchodniCentrum);
        obchodniCentrum.addExit(parkoviste);

        game.getGamePlan().setCurrentArea(parkoviste);

        Assert.assertEquals("Tam můžete pouze jet.", game.processCommand("jdi garáž"));
        Assert.assertEquals("Tam se ale odsud jít nedá.", game.processCommand("jdi drogerie"));
        game.processCommand("jdi obchodní_centrum");  
        Assert.assertEquals("obchodní_centrum", game.getGamePlan().getCurrentArea().getName());
    }
}
