package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro přistoupení k bankomatu {@link CommandGoToATM}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandGoToATMTest
{
    private Game game;

    /**
     * Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandGoToATMTestExecute()
    {
        Area banka= new Area("banka","Pokud nemáte peníze, přistupte k bankomatu a vezměte peníze. Pokud peníze máte, vraťte se na parkoviště. ", AreaType.BANK, false);
        Area drogerie = new Area("drogerie","Pokud již máte koupené mýdlo, vraťte se do obchodu. V opačném případě si vezměte mýdlo. ", AreaType.SHOP, false);

        Item penize = new Item("peníze", "Peníze, které lze použít pro nákup věcí v obchodech.", GetType.TAKEOUT, true);
        Item bankomat = new Item("bankomat", "Bankomat pro výběr peněz.", GetType.OTHER, false);

        drogerie.addItem(penize);
        banka.addItem(penize);
        banka.addItem(bankomat);

        game.getGamePlan().setCurrentArea(drogerie);
        Assert.assertEquals("Předmět 'bankomat' tady není.", game.processCommand("přistup bankomat"));

        game.getGamePlan().setCurrentArea(banka);
        Assert.assertEquals("K peníze nelze přistoupit.", game.processCommand("přistup peníze"));
        Assert.assertEquals("Jsem u bankomatu.", game.processCommand("přistup bankomat"));
    }
}
