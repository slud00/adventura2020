package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro vystoupení z auta {@link CommandGetOutOfCar}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandGetOutOfCarTest
{
    private Game game;

    /**
     * Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandGetOutOfCarTestExecute()
    {
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domu.\n" +  "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);

        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item brufen = new Item("brufen", "Brufen, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item auto = new Item("auto", "Auto, pro přesunování mezi parkovištěm a garáží.", GetType.TAKE, false);

        garaz.addItem(klice);
        garaz.addItem(auto);
        garaz.addItem(brufen);

        game.getGamePlan().setCurrentArea(garaz);
        Assert.assertEquals("Do 'brufen' nelze nastoupit.", game.processCommand("nastup brufen"));
        Assert.assertEquals("Nemáte klíče.", game.processCommand("nastup auto"));

        Assert.assertEquals("Nejste v autě.", game.processCommand("vystup auto"));
        game.processCommand("vezmi klíče");
        Assert.assertEquals("Nastoupil jste do auta.", game.processCommand("nastup auto"));
        Assert.assertEquals("Z 'brufen' nelze vystoupit.", game.processCommand("vystup brufen"));
        Assert.assertEquals("Vystoupil jste z auta.", game.processCommand("vystup auto"));
    }
}

