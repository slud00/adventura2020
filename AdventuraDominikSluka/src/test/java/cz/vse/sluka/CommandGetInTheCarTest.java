package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro nastoupení do auta {@link CommandGetInTheCar}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandGetInTheCarTest
{
    private Game game;

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandGetInTheCarTest()
    {
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domu.\n" +  "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);

        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item brufen = new Item("brufen", "Brufen, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item auto = new Item("auto", "Auto, pro přesunování mezi parkovištěm a garáží.", GetType.TAKE, false);

        garaz.addItem(klice);
        garaz.addItem(auto);
        garaz.addItem(brufen);

        game.getGamePlan().setCurrentArea(garaz);
        Assert.assertEquals("Do 'brufen' nelze nastoupit.", game.processCommand("nastup brufen"));
        Assert.assertEquals("Nemáte klíče.", game.processCommand("nastup auto"));
        game.processCommand("vezmi klíče");
        Assert.assertEquals("Nastoupil jste do auta.", game.processCommand("nastup auto"));
        Assert.assertEquals("Už jste v autě.", game.processCommand("nastup auto"));

    }
}
