package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro třídu zajišťující jízdu autem {@link CommandRide}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandRideTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandRideTestExecute()
    {
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domu.\n" +
                "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);
        Area parkoviste = new Area("parkoviště","Pokud již máte všechny věci koupené, jeďte do garáže.\n" +  
                "Pokud je všechny nemáte, jděte je koupit do obchodu, ale pouze pokud máte peníze. Ty popřípadě jděte vybrat do banky. ", 
                AreaType.PARKING,true); 
        Area obchodniCentrum = new Area("obchodní_centrum","Pokud máte všechny věci, vraťte se na parkoviště. Jinak jděte nakupovat. Pokud máte hlad, jděte do restaurace. ",
                AreaType.OTHER, false);

        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item auto = new Item("auto", "Auto, pro přesunování mezi parkovištěm a garáží.", GetType.TAKE, false);
        Item chleb = new Item("chléb", "Chléb, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item bankomat = new Item("bankomat", "Bankomat pro výběr peněz.", GetType.OTHER, false);      

        garaz.addItem(klice);
        garaz.addItem(auto);

        parkoviste.addItem(chleb);
        parkoviste.addItem(bankomat);

        garaz.addExit(parkoviste);
        parkoviste.addExit(garaz);

        garaz.addExit(obchodniCentrum);
        obchodniCentrum.addExit(garaz);

        game.getGamePlan().setCurrentArea(garaz);

        assertEquals("Předmět 'chléb' tady není.", game.processCommand("vezmi chléb"));
        assertEquals("Předmět 'bankomat' tady není.", game.processCommand("vezmi bankomat"));

        game.processCommand("vezmi klíče");
        assertEquals("Nejste v autě.", game.processCommand("jeď parkoviště"));
        assertEquals("Nastoupil jste do auta.", game.processCommand("nastup auto"));
        assertEquals("Tam se ale odsud jet nedá.", game.processCommand("jeď obchodní_centrum")); 
        game.processCommand("jeď parkoviště");
        assertEquals("parkoviště", game.getGamePlan().getCurrentArea().getName()); 
        assertEquals("Vystoupil jste z auta.", game.processCommand("vystup auto")); 

        assertEquals("Nastoupil jste do auta.", game.processCommand("nastup auto"));
        game.processCommand("jeď garáž");
        assertEquals("garáž", game.getGamePlan().getCurrentArea().getName()); 

        assertEquals("Sebral jste předmět 'chléb' a uložil jste ho do inventáře.", game.processCommand("vezmi chléb"));
        assertEquals("Předmět 'bankomat' tady není.", game.processCommand("vezmi bankomat"));

    }

    private void assertEquals(String s, String vezmi_bankomat) {
    }
}
