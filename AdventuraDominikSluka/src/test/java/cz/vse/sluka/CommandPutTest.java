package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CommandPutTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class CommandPutTest
{
    private Game game;

    /**
     * Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandPutTestExecute()
    {
        Area domov = new Area("domov","Pokud již máte všechny věci ze seznamu, vyložte je a vyhrajete.\n" +  "Pokud všechny nemáte, jděte je sehnat. Aktuálně máte možnost jít do garáže. ", AreaType.HOME, false);
        
        Item mydlo = new Item("mýdlo", "Mýdlo, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item brufen = new Item("brufen", "Brufen, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item chleb = new Item("chléb", "Chléb, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);

        game.getGamePlan().setCurrentArea(domov);

        Assert.assertEquals("chléb není v batohu.",game.processCommand("polož chléb"));

        game.getGamePlan().getBackpack().putInBackpack(mydlo);
        game.getGamePlan().getBackpack().putInBackpack(brufen);
        game.getGamePlan().getBackpack().putInBackpack(chleb);

        game.getGamePlan().setCurrentArea(domov);

        Assert.assertEquals("chléb byl odebrán z batohu.",game.processCommand("polož chléb"));
        Assert.assertFalse(game.getGamePlan().isVictorious(game.getGamePlan().getCurrentArea()));

        Assert.assertEquals("chléb není v batohu.",game.processCommand("polož chléb"));

        Assert.assertEquals("brufen byl odebrán z batohu.",game.processCommand("polož brufen"));
        Assert.assertFalse(game.getGamePlan().isVictorious(game.getGamePlan().getCurrentArea()));

        Assert.assertEquals("Vyhrál jste.",game.processCommand("polož mýdlo"));

        Assert.assertTrue(game.getGamePlan().isVictorious(game.getGamePlan().getCurrentArea()));
    }
}
