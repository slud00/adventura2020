package cz.vse.sluka.test;

import cz.vse.sluka.logika.Backpack;
import cz.vse.sluka.logika.GetType;
import cz.vse.sluka.logika.Item;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro třídu {@link Backpack}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class BackpackTest
{
    private Backpack backpack;
    
    /**
     * Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        backpack = new Backpack(4);
    }
    
    @Test
    public void backpackTestExecute()
    {
        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item mydlo = new Item("mýdlo", "Mýdlo, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item brufen = new Item("brufen", "Brufen, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item chleb = new Item("chléb", "Chléb, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item penize = new Item("peníze", "Peníze, které lze použít pro nákup věcí v obchodech.", GetType.TAKEOUT, true);

        Assert.assertEquals("Batoh je prázdný.", backpack.getBackpackContent());
        Assert.assertFalse(backpack.isItemAtBag("klíče"));
        Assert.assertEquals(null, backpack.getItemFromBag("klíče"));
        Assert.assertEquals("Sebral jste předmět 'klíče' a uložil jste ho do inventáře.",backpack.putInBackpack(klice));
        Assert.assertEquals(true, backpack.isItemAtBag("klíče"));
        Assert.assertEquals("klíče", backpack.getItemFromBag("klíče").getName());
        Assert.assertEquals("Sebral jste předmět 'chléb' a uložil jste ho do inventáře.",backpack.putInBackpack(chleb));
        backpack.putInBackpack(penize);
        Assert.assertEquals("Sebral jste předmět 'brufen' a uložil jste ho do inventáře.",backpack.putInBackpack(brufen));
        Assert.assertEquals("Batoh je plný.",backpack.putInBackpack(mydlo));
    }
}
