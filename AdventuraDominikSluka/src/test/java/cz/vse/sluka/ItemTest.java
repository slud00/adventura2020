package cz.vse.sluka.test;

import cz.vse.sluka.logika.Area;
import cz.vse.sluka.logika.AreaType;
import cz.vse.sluka.logika.GetType;
import cz.vse.sluka.logika.Item;
import org.junit.Assert;
import org.junit.Test;

/**
 * Testovací třída pro třídu {@link Item}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class ItemTest
{
    @Test
    public void testItems()
    {
        Area domov = new Area("domov","Pokud již máte všechny věci ze seznamu, vyložte je a vyhrajete.\n" +  "Pokud všechny nemáte, jděte je sehnat. Aktuálně máte možnost jít do garáže. ", AreaType.HOME, false);
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domu.\n" +  "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);

        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item penize = new Item("peníze", "Peníze, které lze použít pro nákup věcí v obchodech.", GetType.TAKEOUT, true);

        Assert.assertFalse(domov.containsItem(penize.getName()));
        Assert.assertFalse(garaz.containsItem(klice.getName()));
        Assert.assertFalse(domov.containsItem("pc"));

        garaz.addItem(klice);
        domov.addItem(penize);

        Assert.assertTrue(domov.containsItem(penize.getName()));
        Assert.assertTrue(garaz.containsItem(klice.getName()));
        Assert.assertFalse(domov.containsItem("pc"));

        Assert.assertEquals(penize, domov.removeItem(penize.getName()));
        Assert.assertEquals(klice, garaz.removeItem(klice.getName()));
        Assert.assertNull(domov.removeItem("pc"));

        Assert.assertFalse(domov.containsItem(penize.getName()));
        Assert.assertFalse(garaz.containsItem(klice.getName()));
        Assert.assertFalse(domov.containsItem("pc"));

        Assert.assertEquals(AreaType.HOME,domov.getAreaType());
    }  
}
