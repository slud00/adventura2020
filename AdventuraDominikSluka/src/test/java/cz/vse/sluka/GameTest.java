package cz.vse.sluka.test;

import cz.vse.sluka.logika.Game;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro komplexní otestování herního příběhu {@link GameTest}.
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class GameTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void testPlayerQuit()
    {
        Assert.assertEquals("domov", game.getGamePlan().getCurrentArea().getName());
        game.processCommand("jdi garáž");
        Assert.assertEquals("Batoh je prázdný.", game.processCommand("batoh"));

        Assert.assertEquals("Tam můžete pouze jet.",game.processCommand("jdi parkoviště"));
        Assert.assertEquals("Nejste v autě.",game.processCommand("jeď parkoviště"));

        game.processCommand("vezmi klíče");
        game.processCommand("nastup auto");

        game.processCommand("jeď parkoviště");
        Assert.assertEquals("Jste v autě. Nemůžete jít.",game.processCommand("jdi banka"));
        game.processCommand("vystup auto");
        Assert.assertEquals("parkoviště", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi lékárna");
        Assert.assertEquals("Předmět 'chleba' tady není.",game.processCommand("vezmi chleba"));

        Assert.assertEquals("Nejste v autě.",game.processCommand("jeď banka"));
        game.processCommand("jdi banka");
        game.processCommand("přistup bankomat");
        game.processCommand("vezmi peníze");
        game.processCommand("jdi parkoviště");
        game.processCommand("jdi obchodní_centrum");

        game.processCommand("jdi lékárna");
        game.processCommand("vezmi brufen"); 
        game.processCommand("jdi obchodní_centrum");

        game.processCommand("jdi potraviny");
        game.processCommand("vezmi chléb");
        game.processCommand("jdi obchodní_centrum");

        game.processCommand("jdi drogerie");
        Assert.assertEquals("Jste hladový. Jdi se najíst.",game.processCommand("vezmi mýdlo"));
        Assert.assertEquals("Nejste v restauraci.",game.processCommand("najez_se"));
        game.processCommand("jdi obchodní_centrum");

        game.processCommand("jdi restaurace");
        Assert.assertEquals("Dobrou chuť.",game.processCommand("najez_se"));
        game.processCommand("jdi obchodní_centrum");

        game.processCommand("jdi drogerie");
        Assert.assertEquals("Batoh je plný.",game.processCommand("vezmi mýdlo"));

        game.processCommand("jdi obchodní_centrum");
        game.processCommand("jdi parkoviště");
        game.processCommand("polož chléb");

        game.processCommand("jdi obchodní_centrum");

        game.processCommand("jdi drogerie");
        game.processCommand("vezmi mýdlo");

        game.processCommand("jdi obchodní_centrum");
        game.processCommand("jdi parkoviště");
        Assert.assertEquals("Nejste v autě.",game.processCommand("jeď garáž"));
        game.processCommand("nastup auto");
        game.processCommand("jeď garáž");

        Assert.assertFalse(game.isGameOver());
        game.processCommand("vystup auto");
        game.processCommand("jdi domov");

        game.processCommand("polož brufen");
        game.processCommand("polož mýdlo");

        game.processCommand("jdi garáž");
        game.processCommand("vezmi chléb");   

        game.processCommand("jdi domov");
        game.processCommand("polož chléb");

        Assert.assertTrue(game.isGameOver());
    }

}
