package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro třídu zajišťující sebrání předmětu {@link CommandPick}.
 *
 * @author  Dominik Sluka
 * @version LS 2020
 */
public class CommandPickTest
{
    private Game game;
    /**
     * Inicializuje objekty pro testy.
     */
    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void commandPickTestExecute()
    {
        Area domov = new Area("domov","Pokud již máte všechny věci ze seznamu, vyložte je a vyhrajete.\n" +  "Pokud všechny nemáte, jděte je sehnat. Aktuálně máte možnost jít do garáže. ", AreaType.HOME, false);
        Area banka = new Area("banka","Pokud nemáte peníze, jděte k bankomatu a vezměte peníze. Pokud peníze máte, vraťte se na parkoviště. ", AreaType.BANK, false);
        Area drogerie = new Area("drogerie","Pokud již máte koupené mýdlo, vraťte se do obchodu. V opačném případě si vezměte mýdlo. ", AreaType.SHOP, false);

        Item klice = new Item("klíče", "Klíče k autu.", GetType.TAKE);
        Item penize = new Item("peníze", "Peníze, které lze použít pro nákup věcí v obchodech.", GetType.TAKEOUT, true);
        Item mydlo = new Item("mýdlo", "Mýdlo, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item brufen = new Item("brufen", "Brufen, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item chleb = new Item("chléb", "Chléb, položka z nákupního seznamu, kterou třeba donést domů.", GetType.BUY);
        Item auto = new Item("auto", "Auto, pro přesunování mezi parkovištěm a garáží.", GetType.TAKE, false);
        Item bankomat = new Item("bankomat", "Bankomat pro výběr peněz.", GetType.OTHER, false);

        domov.addItem(klice);
        domov.addItem(auto);
        domov.addItem(penize);
        domov.addItem(mydlo);
        domov.addItem(brufen);
        domov.addItem(chleb);

        banka.addItem(bankomat);
        banka.addItem(penize);

        drogerie.addItem(mydlo);

        game.getGamePlan().setCurrentArea(drogerie);
        Assert.assertEquals("Nemáte peníze.", game.processCommand("vezmi mýdlo"));

        game.getGamePlan().setCurrentArea(banka);
        Assert.assertEquals("Nejste u bankomatu. Nemůžete vybrat peníze.", game.processCommand("vezmi peníze"));

        game.getGamePlan().setCurrentArea(domov);
        Assert.assertEquals("Sebral jste předmět 'klíče' a uložil jste ho do inventáře.", game.processCommand("vezmi klíče"));
        Assert.assertEquals("Předmět 'auto' fakt neuneseš.", game.processCommand("vezmi auto"));
        Assert.assertEquals("Sebral jste předmět 'brufen' a uložil jste ho do inventáře.", game.processCommand("vezmi brufen"));
        Assert.assertEquals("Sebral jste předmět 'peníze' a uložil jste ho do inventáře.", game.processCommand("vezmi peníze"));

        game.getGamePlan().setCurrentArea(drogerie);
        Assert.assertEquals("Sebral jste předmět 'mýdlo' a uložil jste ho do inventáře.", game.processCommand("vezmi mýdlo"));
    }
}
