package cz.vse.sluka.test;

import cz.vse.sluka.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovací třída pro komplexní otestování třídy {@link ListOfCommands}.
 *
 * @author Luboš Pavlíček
 * @author Jan Říha
 * @version LS 2020
 */
public class ListOfCommandsTest
{
    private Game game;
    private ICommand cmdTerminate;
    private ICommand cmdMove;

    @Before
    public void setUp()
    {
        game = new Game();
        cmdTerminate = new CommandTerminate(game);
        cmdMove = new CommandMove(game.getGamePlan());
    }

    @Test
    public void testAddAndGetCommand()
    {
        ListOfCommands listOfCommands = new ListOfCommands();
        listOfCommands.addCommand(cmdTerminate);
        listOfCommands.addCommand(cmdMove);

        Assert.assertEquals(cmdTerminate, listOfCommands.getCommand(cmdTerminate.getName()));
        Assert.assertEquals(cmdMove, listOfCommands.getCommand(cmdMove.getName()));
        Assert.assertNull(listOfCommands.getCommand("napoveda"));
    }

    @Test
    public void testCheckCommand()
    {
        ListOfCommands listOfCommands = new ListOfCommands();
        listOfCommands.addCommand(cmdTerminate);
        listOfCommands.addCommand(cmdMove);

        Assert.assertTrue(listOfCommands.checkCommand(cmdTerminate.getName()));
        Assert.assertTrue(listOfCommands.checkCommand(cmdMove.getName()));

        if (cmdTerminate.getName().substring(0, 1).equals(cmdTerminate.getName().substring(0, 1).toLowerCase())) {
            Assert.assertFalse(listOfCommands.checkCommand(cmdTerminate.getName().toUpperCase()));
        } else {
            Assert.assertFalse(listOfCommands.checkCommand(cmdTerminate.getName().toLowerCase()));
        }

        Assert.assertFalse(listOfCommands.checkCommand("napoveda"));
    }

    @Test
    public void testGetNames()
    {
        ListOfCommands listOfCommands = new ListOfCommands();
        listOfCommands.addCommand(cmdTerminate);
        listOfCommands.addCommand(cmdMove);

        String names = listOfCommands.getNames();
        Assert.assertTrue(names.contains(cmdTerminate.getName()));
        Assert.assertTrue(names.contains(cmdMove.getName()));

        if (cmdTerminate.getName().substring(0, 1).equals(cmdTerminate.getName().substring(0, 1).toLowerCase())) {
            Assert.assertFalse(names.contains(cmdTerminate.getName().toUpperCase()));
        } else {
            Assert.assertFalse(names.contains(cmdTerminate.getName().toLowerCase()));
        }

        Assert.assertFalse(names.contains("napoveda"));

        Assert.assertEquals(2, names.split(" ").length);
    }
}
