package cz.vse.sluka.test;

import cz.vse.sluka.logika.Area;
import cz.vse.sluka.logika.AreaType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Testovací třída pro komplexní otestování třídy {@link Area}.
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Dominik Sluka
 * @version LS 2020
 */
public class AreaTest
{
    @Test
    public void testAreaExits()
    {

        Area domov = new Area("domov","Pokud již máte všechny věci ze seznamu, vyložte je a vyhrajete.\n" +  "Pokud všechny nemáte, jděte je sehnat. Aktuálně máte možnost jít do garáže. ", AreaType.HOME, false);
        Area garaz = new Area("garáž", "Máte možnost nastoupit do auta a odjet na parkoviště obchodu. Nebo můžete jít domu.\n" +  "Pokud jste již dovezli věci z obchodu, doneste je všechny domů. ", AreaType.GARAGE, true);

        domov.addExit(garaz);
        garaz.addExit(domov);

        Assert.assertEquals(garaz, domov.getExitArea(garaz.getName()));
        Assert.assertEquals(domov, garaz.getExitArea(domov.getName()));

        Assert.assertNull(garaz.getExitArea("pokoj"));
        Assert.assertNull(domov.getExitArea("pokoj"));
    }

}
